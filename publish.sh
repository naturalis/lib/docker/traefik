#!/bin/sh
set -eu

# shellcheck disable=SC2124
versions="$@"
for version in $versions; do
    image="$CI_REGISTRY_IMAGE:build_$version-$CI_COMMIT_REF_SLUG"

    docker pull "$image"

    docker tag "$image" "$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID-$version"
    docker push "$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID-$version"

    if [ "$CI_COMMIT_BRANCH" = "main" ]; then
      docker tag "$image" "$CI_REGISTRY_IMAGE:$version"
      docker push "$CI_REGISTRY_IMAGE:$version"
    fi
done
