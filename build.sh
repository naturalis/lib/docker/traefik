#!/bin/sh
set -eu

# shellcheck disable=SC2124
versions="$@"

basedir=$(dirname "$0")

for version in $versions; do
  docker build --build-arg TRAEFIK_VERSION="$version" --tag "$CI_REGISTRY_IMAGE:build_$version-$CI_COMMIT_REF_SLUG" "$basedir"
  docker push "$CI_REGISTRY_IMAGE:build_$version-$CI_COMMIT_REF_SLUG"
done
