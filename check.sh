#!/bin/sh
set -eu

# shellcheck disable=SC2124
versions="$@"
for version in $versions; do
    trivy --cache-dir .trivy/ image "$CI_REGISTRY_IMAGE:build_$version-$CI_COMMIT_REF_SLUG"
    trivy --cache-dir .trivy/ image --ignore-unfixed --exit-code 1 --severity HIGH,CRITICAL "$CI_REGISTRY_IMAGE:build_$version-$CI_COMMIT_REF_SLUG"
done
