ARG TRAEFIK_VERSION=latest
FROM traefik:${TRAEFIK_VERSION}

ENV AWS_REGION="eu-central-1"

# hadolint ignore=DL3017
RUN apk update && apk upgrade --no-cache
