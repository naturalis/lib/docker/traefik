# Traefik

Traefik is a web proxy used for many of the docker-compose setups at Naturalis.

## Multiple build

The base image for this project is build multiple times for each stable version.
At the moment we build:

 - v2.11
 - v3.1
 - v3.2
 - latest

Each of these versions are automatically build and published daily.

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

```
pre-commit autoupdate
pre-commit install
```
